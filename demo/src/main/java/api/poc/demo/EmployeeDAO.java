package api.poc.demo;

import java.util.List;
import api.poc.demo.Employee;

public interface EmployeeDAO {
    List<Employee> get();
    Employee get(int id);
    void save(Employee employee);
    void delete(int id);

}
