package api.poc.demo;

import jakarta.persistence.*;

@Entity
@Table(name = "tbl_employee")

public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column
    private String nameE;
    @Column
    private String gender;
    @Column
    private String departmente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameE() {
        return nameE;
    }

    public void setNameE(String nameE) {
        this.nameE = nameE;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDepartmente() {
        return departmente;
    }

    public void setDepartmente(String departmente) {
        this.departmente = departmente;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", nameE='" + nameE + '\'' +
                ", gender='" + gender + '\'' +
                ", departmente='" + departmente + '\'' +
                '}';
    }
}
